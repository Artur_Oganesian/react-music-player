import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  tracks: [],
};
const musicSlice = createSlice({
  name: "tracks",
  initialState,
  reducers: {
    updatedMusicData: (state, action) => {
      state.tracks.push(...action.payload);
    },
  },
});

export const { updatedMusicData } = musicSlice.actions;
export default musicSlice.reducer;
