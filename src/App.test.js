import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import App from "./App";

const mockStore = configureStore([]);

describe("App", () => {
  it("redner without error", () => {
    const store = mockStore({
      musicCollection: {
        tracks: [],
      },
    });

    render(
      <Provider store={store}>
        <App />
      </Provider>,
    );
  });
});
