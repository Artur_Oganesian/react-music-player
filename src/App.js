import React from "react";
import AudioHub from "./components/audioHub/AudioHub";
import SongList from "./components/songList/SongList";
import MusicUploadForm from "./components/musicUploadForm/MusicUploadForm";
import "./global.css";

function App() {
  return (
    <div className="app">
      <AudioHub />
      <main>
        <SongList />
        <MusicUploadForm />
      </main>
    </div>
  );
}

export default App;
