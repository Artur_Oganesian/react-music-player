import React from "react";
import { render, fireEvent } from "@testing-library/react";
import SongRow from "./SongRow";

describe("SongRow", () => {
  const testSongName = "TestArtist-TestSong.mp3";
  const testTrackNumber = 1;

  it("right render with comming props", () => {
    const { getByText } = render(
      <SongRow name={testSongName} trackNumber={testTrackNumber} />,
    );

    expect(getByText(/TestSong/i)).toBeInTheDocument();
    expect(getByText(/TestArtist/i)).toBeInTheDocument();
    expect(getByText(/2/i)).toBeInTheDocument(); // Трек номер + 1
  });

  it("change color of heart symbol by click", () => {
    const { getByText } = render(
      <SongRow name={testSongName} trackNumber={testTrackNumber} />,
    );
    const heartSymbol = getByText(/♥/i);

    expect(heartSymbol).toHaveStyle("color: gray");

    fireEvent.click(heartSymbol);

    expect(heartSymbol).toHaveStyle("color: tomato");
  });

  it("изменяет цвет символа галочки при клике", () => {
    const { getByTestId } = render(
      <SongRow name={testSongName} trackNumber={testTrackNumber} />,
    );
    const checkSymbol = getByTestId("check-symbol");

    expect(checkSymbol).toHaveStyle("color: gray");

    fireEvent.click(checkSymbol);

    expect(checkSymbol).toHaveStyle("color: lightgreen");
  });
});
