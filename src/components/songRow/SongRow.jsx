import React from "react";
import { useState } from "react";
import "./songRow.css";

const SongRow = ({ name, trackNumber }) => {
  const [heartSymbolEffect, setHeartSymbolEffect] = useState(false);
  const [checkSymbolEffect, setCheckSymbolEffect] = useState(false);

  const specialSymbol = "-";
  const index = name.indexOf(specialSymbol);
  const songName = name
    .slice(0, name.length - 4)
    .split("")
    .filter((elem) => elem !== "-")
    .join("")
    .substring(name.length, index);
  const artistName = name.substring(0, index);
  const musicFormat = name
    .split("")
    .filter((elem) => elem !== ".")
    .reverse()
    .splice(0, 3)
    .reverse()
    .join("")
    .toUpperCase();
  return (
    <section className="song-row-container">
      <div className="track-info">
        <span className="track-row-song-name">
          {" "}
          {songName.length < 15 ? songName : songName.slice(0, 10)}
        </span>
        <span className="track-row-artist-name">{artistName}</span>
        <span className="track-row-number">{trackNumber + 1}</span>

        <div className="song-row-symbols">
          <span
            onClick={() => {
              setHeartSymbolEffect(!heartSymbolEffect);
            }}
            style={heartSymbolEffect ? { color: "tomato" } : { color: "gray" }}
          >
            &#9829;
          </span>
          <span
            data-testid="check-symbol"
            onClick={() => {
              setCheckSymbolEffect(!checkSymbolEffect);
            }}
            style={
              checkSymbolEffect ? { color: "lightgreen" } : { color: "gray" }
            }
          >
            &#10004;
          </span>
          <span>&#10150;</span>
          <details>
            <summary></summary>
            <span>Music Format: {musicFormat}</span>
          </details>
        </div>
      </div>
    </section>
  );
};

export default SongRow;
