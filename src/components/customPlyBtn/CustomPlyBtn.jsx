import React from "react";
import { useState, useRef } from "react";
import "./customPlyBtn.css";
const CustomPlyBtn = ({ src }) => {
  const [isPlaying, setIsPlaying] = useState(false);
  const songRef = useRef(null);
  const playSong = () => {
    if (isPlaying) {
      songRef.current.pause();
    } else {
      songRef.current.play();
    }
    setIsPlaying(!isPlaying);
  };
  return (
    <section className="custom-btn-container">
      <button onClick={playSong}>
        {isPlaying ? <>&#9724; </> : <>&#9654;</>}
      </button>
      <audio ref={songRef} src={src} playsInline />
    </section>
  );
};

export default CustomPlyBtn;
