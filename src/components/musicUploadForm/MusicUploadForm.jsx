import React from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { updatedMusicData } from "../../features/music/musicSlice";
import "./musicUploadForm.css";

const MusicUploadForm = () => {
  const [musicFiles, setMusicFiles] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useDispatch();

  const handleFileChange = ({ target: { files } }) => {
    setMusicFiles(files);
  };

  const addTrack = () => {
    const updatedMusicArray = Array.from(musicFiles).map((file) => {
      return {
        audioName: file.name,
        url: URL.createObjectURL(file),
      };
    });
    if (updatedMusicArray.length !== 0) {
      setIsLoading(!isLoading);
      setTimeout(() => {
        setIsLoading(false);
      }, 800);
      dispatch(updatedMusicData(updatedMusicArray));
    } else {
      alert("For the first please upload music");
    }
  };
  return (
    <section className="uplaod-form-container">
      <h2>Music Uplaod Form</h2>
      <input
        type="file"
        accept="audio/*"
        multiple
        onChange={handleFileChange}
      />
      <button onClick={addTrack}>Add Music</button>
      {isLoading && (
        <svg
          className="add-song-loading"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 200 200"
        >
          <linearGradient id="a11">
            <stop offset="0" stopColor="black" stop-opacity="0"></stop>
            <stop offset="1" stopColor="gray"></stop>
          </linearGradient>
          <circle
            fill="none"
            stroke="url(#a11)"
            stroke-width="15"
            stroke-linecap="round"
            stroke-dasharray="0 44 0 44 0 44 0 44 0 360"
            cx="100"
            cy="100"
            r="70"
            transform-origin="center"
          >
            <animateTransform
              type="rotate"
              attributeName="transform"
              calcMode="discrete"
              dur="2"
              values="360;324;288;252;216;180;144;108;72;36"
              repeatCount="indefinite"
            ></animateTransform>
          </circle>
        </svg>
      )}
    </section>
  );
};

export default MusicUploadForm;
