import React from "react";
import { render } from "@testing-library/react";
import MusicUploadForm from "./MusicUploadForm";
import { Provider } from "react-redux";
import { createStore } from "redux";

const testReducer = (state = {}, action) => state;

const store = createStore(testReducer);

describe("MusicUploadForm", () => {
  it("render without error", () => {
    render(
      <Provider store={store}>
        <MusicUploadForm />
      </Provider>,
    );
  });
});
