import React from "react";
import { useSelector } from "react-redux";
import SongRow from "../songRow/SongRow";
import uniqid from "uniqid";
import "./songList.css";
import CustomPlyBtn from "../customPlyBtn/CustomPlyBtn";
const SongList = () => {
  const musicData = useSelector((state) => state.musicCollection.tracks);
  return (
    <section className="song-list-container">
      <div className="song-list-navbar">
        <div className="song-info-title ply_btn_title">
          <h3>{""}</h3>
        </div>
        <div className="song-info-title song-name">
          <h3>Song Name</h3>
        </div>

        <div className="song-info-title artist-name">
          <h3>Artist Name</h3>
        </div>

        <div className="song-info-title track-number">
          <h3>Track</h3>
        </div>
        <div className="song-info-title details_title">
          <h3>{""}</h3>
        </div>
      </div>
      {musicData?.map((file, index, data) => (
        <div key={uniqid()}>
          <CustomPlyBtn src={data[index].url} />
          <SongRow name={file.audioName} trackNumber={index} />
        </div>
      ))}
    </section>
  );
};

export default SongList;
