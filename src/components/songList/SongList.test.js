import React from "react";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import { createStore } from "redux";
import SongList from "./SongList";

const testReducer = (state = { musicCollection: { tracks: [] } }, action) => {
  switch (action.type) {
    default:
      return state;
  }
};

const store = createStore(testReducer);

describe("SongList", () => {
  it("redner without error", () => {
    render(
      <Provider store={store}>
        <SongList />
      </Provider>,
    );
  });
});
