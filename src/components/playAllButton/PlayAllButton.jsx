import React from "react";
import { useState } from "react";
import "./playAllButton.css";

const PlayAllButton = () => {
  const [recordMusic, setRecordMusic] = useState(false);
  return (
    <section className="play_container">
      <button
        type="button"
        className="play_button"
        onClick={() => {
          setRecordMusic(!recordMusic);
        }}
      >
        <span>{recordMusic ? <>&#9654;</> : <>&#9724; </>}</span>
        &nbsp;&nbsp;{recordMusic ? "Play All" : "Stop All"}
      </button>
      <span className="play_details_icon">&#9660;</span>
    </section>
  );
};

export default PlayAllButton;
