import React from "react";
import { render, fireEvent } from "@testing-library/react";
import PlayAllButton from "./PlayAllButton";

describe("PlayAllButton", () => {
  it("renders without crashing", () => {
    render(<PlayAllButton />);
  });

  it("toggles play and stop text on click", () => {
    const { getByText, queryByText } = render(<PlayAllButton />);

    let playButton = queryByText(/Play All/i);
    let stopButton = queryByText(/Stop All/i);

    if (playButton) {
      fireEvent.click(playButton);
      stopButton = getByText(/Stop All/i);
      expect(stopButton).toBeTruthy();
    } else if (stopButton) {
      fireEvent.click(stopButton);
      playButton = getByText(/Play All/i);
      expect(playButton).toBeTruthy();
    } else {
      throw new Error("Button not found");
    }
  });
});
