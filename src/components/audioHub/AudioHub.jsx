import React from "react";
import AddAllButton from "../addAllButton/AddAllButton";
import PlayAllButton from "../playAllButton/PlayAllButton";
import "./audioHub.css";
const AudioHub = () => {
  return (
    <section className="audio_hub_container">
      <div className="hub_add_play_container">
        <PlayAllButton />
        <AddAllButton />
      </div>
      <div className="hub_search_container">
        <div className="hub_track_btn">
          <button>
            <span className="track_icon">&#8593;&#8595;</span>
            <span className="track_title">Track&nbsp;Num...</span>
          </button>
          <span className="track_details_icon">&#9660;</span>
        </div>
        <input type="text" placeholder="Filter" className="search_bar" />{" "}
      </div>
    </section>
  );
};

export default AudioHub;
