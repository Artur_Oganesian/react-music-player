import React from "react";
import { render } from "@testing-library/react";
import AudioHub from "./AudioHub";

jest.mock("../addAllButton/AddAllButton", () => {
  return function DummyAddAllButton() {
    return <div>AddAllButtonMock</div>;
  };
});

jest.mock("../playAllButton/PlayAllButton", () => {
  return function DummyPlayAllButton() {
    return <div>PlayAllButtonMock</div>;
  };
});

describe("AudioHub Component", () => {
  it("contains AddAllButton and PlayAllButton", () => {
    const { getByText } = render(<AudioHub />);
    expect(getByText("AddAllButtonMock")).toBeInTheDocument();
    expect(getByText("PlayAllButtonMock")).toBeInTheDocument();
  });
});
