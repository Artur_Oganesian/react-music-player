import React from "react";
import "./addAllButton.css";
const AddAllButton = () => {
  return (
    <section className="add_container">
      <span className="add_icon">&#10133;</span>
      <button type="button" className="add_button">
        Add All
      </button>
      <span className="add_details_icon">&#9660;</span>
    </section>
  );
};

export default AddAllButton;
