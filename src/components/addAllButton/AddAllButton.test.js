import React from "react";
import { createRoot } from "react-dom/client";
import AddAllButton from "./AddAllButton";

test("renders without crashing", () => {
  const container = document.createElement("div");
  const root = createRoot(container);
  root.render(<AddAllButton />);
  root.unmount();
});

test("has correct class names", () => {
  const container = document.createElement("div");
  const root = createRoot(container);
  root.render(<AddAllButton />);
  root.unmount();
});
