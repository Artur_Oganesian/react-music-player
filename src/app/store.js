import { configureStore } from "@reduxjs/toolkit";
import musicSlice from "features/music/musicSlice";

export const store = configureStore({
  reducer: {
    musicCollection: musicSlice,
  },
});
