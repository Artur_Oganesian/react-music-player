# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### project structure
music-player
├─ .git
│  ├─ COMMIT_EDITMSG
│  ├─ config
│  ├─ description
│  ├─ HEAD
│  ├─ hooks
│  │  ├─ applypatch-msg.sample
│  │  ├─ commit-msg.sample
│  │  ├─ fsmonitor-watchman.sample
│  │  ├─ post-update.sample
│  │  ├─ pre-applypatch.sample
│  │  ├─ pre-commit.sample
│  │  ├─ pre-merge-commit.sample
│  │  ├─ pre-push.sample
│  │  ├─ pre-rebase.sample
│  │  ├─ pre-receive.sample
│  │  ├─ prepare-commit-msg.sample
│  │  ├─ push-to-checkout.sample
│  │  ├─ sendemail-validate.sample
│  │  └─ update.sample
│  ├─ index
│  ├─ info
│  │  └─ exclude
│  ├─ logs
│  │  ├─ HEAD
│  │  └─ refs
│  │     ├─ heads
│  │     │  └─ main
│  │     └─ remotes
│  │        └─ origin
│  │           ├─ create-ui-for-global-use
│  │           ├─ dev
│  │           └─ main
│  ├─ objects
│  │  ├─ 04
│  │  │  └─ 12ae8f7f8e1768c78b96e74878188387502330
│  │  ├─ 05
│  │  │  ├─ 34c93b3f0651787ea34d730005b1b0f19af7f9
│  │  │  └─ e346e0e265fdba41d3acffd38d0295f225f808
│  │  ├─ 06
│  │  │  └─ 01ea33940e64c63ac3e3f8689eb07c525221b7
│  │  ├─ 08
│  │  │  ├─ 0077e8a847d7acbb26ac15e08285d12e63a682
│  │  │  ├─ 0d6c77ac21bb2ef88a6992b2b73ad93daaca92
│  │  │  └─ 9956b0093d2e9d2dcca6774350fb6dbbbc9e32
│  │  ├─ 0c
│  │  │  └─ da51632592b3b297fe0cc50a6aa6080e4b6a0d
│  │  ├─ 16
│  │  │  └─ 1d2773bd018da48fbba42f28ad640554614d5d
│  │  ├─ 1a
│  │  │  └─ d4ac23a16565085fd19b5773eaf78175df0c6e
│  │  ├─ 1b
│  │  │  └─ ce3fae47fb864366c6141416ef47314e6bf7fe
│  │  ├─ 1c
│  │  │  └─ 6ca3e246fd6b98cfc0c3a846c0034a7f75e83b
│  │  ├─ 1d
│  │  │  ├─ cd63aabf9882e2a5c61c818923397391ee1a79
│  │  │  └─ f3ef75b770ba61cb16d3faa6e15bfd2e30bfb2
│  │  ├─ 1f
│  │  │  ├─ 03afeece5ac28064fa3c73a29215037465f789
│  │  │  └─ 2f141fafdeb1d31d85b008ec5132840c5e6362
│  │  ├─ 21
│  │  │  └─ c284b878fda337119af7caa8da81a8b8b2b4d9
│  │  ├─ 24
│  │  │  ├─ 9e9eeae80d3610dbc09656a2b6af732758b448
│  │  │  └─ d8e6b2a5fd175229528638ce3ad32149cf39b5
│  │  ├─ 25
│  │  │  ├─ 9570311dffd30265c276f2715af797314abfb2
│  │  │  └─ ff436b4c54b118eae51011d97173683125981d
│  │  ├─ 2d
│  │  │  └─ af95273f931d64c2e21d9a28ba5344b8656c85
│  │  ├─ 2e
│  │  │  ├─ 6bf0a9b537b63abc32ac0b753366799d46e41d
│  │  │  └─ e64b8ed2c0044c7fc172b1a7663eab172fedb0
│  │  ├─ 32
│  │  │  └─ 6d9d7e409600abdea508a02d6b5ddd57463373
│  │  ├─ 35
│  │  │  └─ c394b2b6438cec2b4732946ce78bf29aa990ca
│  │  ├─ 37
│  │  │  └─ 845757234ccb68531c10cf7a2ffc589c47e342
│  │  ├─ 3a
│  │  │  ├─ c7d453022e63da188422e4f8e8ef9d7294381e
│  │  │  └─ fe030f8854ded1295c4686984c93e1177815d5
│  │  ├─ 3b
│  │  │  └─ ba0a61817b4c8c1a46bfc2cf9fa1824226227e
│  │  ├─ 41
│  │  │  └─ ef8897c2acb828c6d3f829a704e6842ca91a2a
│  │  ├─ 46
│  │  │  └─ 4d7a46da1ac7e3be64d63a5bbd03e3a3d3bb70
│  │  ├─ 4d
│  │  │  ├─ 183295fcbb46da4bf0e1e6fc36f570822ffcef
│  │  │  └─ 29575de80483b005c29bfcac5061cd2f45313e
│  │  ├─ 4e
│  │  │  └─ 28f81a9f21c15b6692319bc1cf371868ad645b
│  │  ├─ 4f
│  │  │  └─ ce557cee05c6bc1ae822fd32e88163cd6f6e31
│  │  ├─ 52
│  │  │  ├─ 53d3ad9e6be6690549cb255f5952337b02401d
│  │  │  └─ 578f356fcc6f9528a5c3e936bcd7ba2adbc80d
│  │  ├─ 54
│  │  │  └─ b743f2c37cdc55aee4ed1a1c4482cebb17bfbd
│  │  ├─ 56
│  │  │  └─ f27a0a71fbb33857dca698ac2968c10c1ee6d5
│  │  ├─ 58
│  │  │  └─ beeaccd87e230076cab531b8f418f40b6d1aeb
│  │  ├─ 5e
│  │  │  ├─ 1ef598c6d930b640bcbb9192ae8932d5238860
│  │  │  └─ 21d4d82131a7e417345fb1b2e091b7e02d3a14
│  │  ├─ 5f
│  │  │  └─ 965d15024da2f38e6221dc34ad2cf9a1ca8b7a
│  │  ├─ 62
│  │  │  └─ 15e5623e0de8f055dcbc26c0577893325f3965
│  │  ├─ 63
│  │  │  ├─ 01556f92ab6cf1ca963a3a8c0da078f7181eed
│  │  │  └─ d43ff8af9bf2211c378dd6d0f8a0d251554642
│  │  ├─ 66
│  │  │  └─ e0e6d2d5364e408509d0c01d15c03007507236
│  │  ├─ 6c
│  │  │  └─ e2a2d5046649367144d452a9bb53a7067e1209
│  │  ├─ 6d
│  │  │  └─ 17417f206291fbf4c74798bcc3c62b36fad85a
│  │  ├─ 6e
│  │  │  └─ 4c70500668ffb0b91750aef403e373dd47fb5c
│  │  ├─ 70
│  │  │  └─ d7804c5cfc66e91789260674027da9da8026ea
│  │  ├─ 74
│  │  │  └─ b5e053450a48a6bdb4d71aad648e7af821975c
│  │  ├─ 7b
│  │  │  └─ 1e172334cdc980bee9ad6a8a87cf21e0ed18d8
│  │  ├─ 7e
│  │  │  └─ 826687eb444d3eb7bed025abe737134ec07ec0
│  │  ├─ 80
│  │  │  └─ 3f566b9a7a17acb551101f388de2d6185938be
│  │  ├─ 81
│  │  │  └─ 8e74f853380d7fbbb5b630fcf6c0eb4f6b82e6
│  │  ├─ 82
│  │  │  └─ d637ce4d41fc51100c44847331cb46e1ad0b6e
│  │  ├─ 85
│  │  │  └─ 87a0f5050e480c01a13d7a1fd00270e9e6b589
│  │  ├─ 88
│  │  │  └─ 53efeb094b39cdc3cab17475183bccda8751d6
│  │  ├─ 8e
│  │  │  ├─ 1a26e7c050c6709d6ca3e8daf7d42f8107844c
│  │  │  └─ 29b36dea7f04ae8729d8b33ecc05c3c9b0fe46
│  │  ├─ 8f
│  │  │  ├─ 2609b7b3e0e3897ab3bcaad13caf6876e48699
│  │  │  ├─ 5d307ad2ccbc52fb48ab202bf06dac0396ba77
│  │  │  └─ be996de48e47fb9e3f8208ec72ad656e9ab4ee
│  │  ├─ 9a
│  │  │  └─ b835c5fe5b12ebb4bea53200eb546a52e5e712
│  │  ├─ 9c
│  │  │  └─ 6db3e9fffdb606843699a78d52754c06e69308
│  │  ├─ 9d
│  │  │  └─ fc1c058cebbef8b891c5062be6f31033d7d186
│  │  ├─ a0
│  │  │  └─ 84d4dc7815861b03f233ba37c8fb56cccd2efe
│  │  ├─ a1
│  │  │  └─ 1777cc471a4344702741ab1c8a588998b1311a
│  │  ├─ a4
│  │  │  ├─ 6d938cf25403d10ab6f383eab3e89a2902bd67
│  │  │  └─ e47a6545bc15971f8f63fba70e4013df88a664
│  │  ├─ aa
│  │  │  ├─ 069f27cbd9d53394428171c3989fd03db73c76
│  │  │  └─ 6b2e33a39203f088b66d3ea65592da704725ab
│  │  ├─ ac
│  │  │  └─ f180123b94b6c21535f4183dce81a3e855defd
│  │  ├─ b6
│  │  │  ├─ 359b44ca158565f77651dcbdf44d35bcb1e78d
│  │  │  ├─ ed278bffd7d13e3a8afdae6e372bccc2017cb2
│  │  │  └─ fbbc05b62cb6fade5075a601dd31dc095ce841
│  │  ├─ b7
│  │  │  └─ 51b9961bed0dc6b660d7e932770d633a996f18
│  │  ├─ b9
│  │  │  └─ 707e8bef9abde33367cf1fa7a44229aef75c15
│  │  ├─ ba
│  │  │  └─ cb4e463520b6e1cfe50eff29a4f379a4b00eea
│  │  ├─ bc
│  │  │  └─ 7d97dacc6220769173e4180f968cc0535a6f29
│  │  ├─ be
│  │  │  └─ 89af80f35e8680a8ce9e4f16828d3c0f25487a
│  │  ├─ bf
│  │  │  └─ 2fae1e9ae0c6b8745b2bf52b5fc00e2062487d
│  │  ├─ c7
│  │  │  ├─ 01b0d26a1ed8487d2925b83579eba75cbe31de
│  │  │  └─ 427e9ccf0b7ae4ea610935d3c42cec02013f97
│  │  ├─ c8
│  │  │  └─ 60ef2542bc54ae37dc9b0daf9b2e39551576b1
│  │  ├─ d0
│  │  │  └─ 66a9791dce4f3b8860524bbcbcbb91dc2bf3fa
│  │  ├─ d3
│  │  │  └─ 4965146105fdc5fbca14e5479aa23580341378
│  │  ├─ d5
│  │  │  ├─ 63c0fb10ba0e42724b21286eb546ee4e5734fc
│  │  │  └─ 91f27bd1001d9f2cc43cb55ebc18d941333a50
│  │  ├─ d9
│  │  │  └─ dee0631390787fe25002f2fa4e9d13633c0e80
│  │  ├─ da
│  │  │  └─ 7c6202c2ab1298333809bebffcfb4faf137428
│  │  ├─ db
│  │  │  └─ 6e7b51a582e59932883ff8bbce4ad1ac765966
│  │  ├─ dc
│  │  │  └─ 2946513c84c9216adc9c1d2f423f51df21a9b0
│  │  ├─ dd
│  │  │  └─ ece44f16c6f8d5d2622e0ae65e36004263632e
│  │  ├─ de
│  │  │  ├─ 44ba2575f6297664e3d93f359f7802d257efec
│  │  │  └─ b173c0039d6d5b4df0a3483be8449f0b4226d3
│  │  ├─ df
│  │  │  └─ 58093a6ec8fa94bbe1a57dcc10ab9ef19aba65
│  │  ├─ e1
│  │  │  └─ 7b89b4f13d4e512cdd29ac94dae45553c58ad0
│  │  ├─ e3
│  │  │  └─ 5e3daeead3c8b4cd0dadfaf4456fcd35db8a82
│  │  ├─ e6
│  │  │  └─ 9de29bb2d1d6434b8b29ae775ad8c2e48c5391
│  │  ├─ e8
│  │  │  └─ ca8aa71ea2e9aa5c890d39f5fc042627941f0d
│  │  ├─ e9
│  │  │  └─ e57dc4d41b9b46e05112e9f45b7ea6ac0ba15e
│  │  ├─ ea
│  │  │  └─ 7cb7abecc0e7f26f9fd281e545ae60114a4f07
│  │  ├─ eb
│  │  │  └─ 2c4252a7ad2b30c892748ed9f7291bafefe1a8
│  │  ├─ ec
│  │  │  └─ 2585e8c0bb8188184ed1e0703c4c8f2a8419b0
│  │  ├─ f1
│  │  │  └─ a62dbe5144e42839a14cc4ba75abefb2671031
│  │  ├─ f4
│  │  │  └─ 054fcd830e149a43b96db9f06458202bc541a7
│  │  ├─ fa
│  │  │  └─ 8e50b8d323f948c6a2a6c52834c9539f926b6b
│  │  ├─ fc
│  │  │  ├─ 44b0a3796c0e0a64c3d858ca038bd4570465d9
│  │  │  └─ af1dbc8ae958124532258dbc57c5537a166cff
│  │  ├─ ff
│  │  │  └─ e282203d8e9d20e48c4524de2a6697ae0ed4fb
│  │  ├─ info
│  │  └─ pack
│  ├─ ORIG_HEAD
│  ├─ packed-refs
│  └─ refs
│     ├─ heads
│     │  └─ main
│     ├─ remotes
│     │  └─ origin
│     │     ├─ create-ui-for-global-use
│     │     ├─ dev
│     │     └─ main
│     └─ tags
├─ .gitignore
├─ .prettierignore
├─ babel.config.js
├─ jest.config.js
├─ jsconfig.json
├─ package-lock.json
├─ package.json
├─ public
│  ├─ favicon.ico
│  ├─ index.html
│  └─ manifest.json
├─ README.md
├─ src
│  ├─ app
│  │  └─ store.js
│  ├─ App.js
│  ├─ App.test.js
│  ├─ components
│  │  ├─ addAllButton
│  │  │  ├─ addAllButton.css
│  │  │  ├─ AddAllButton.jsx
│  │  │  └─ AddAllButton.test.js
│  │  ├─ audioHub
│  │  │  ├─ audioHub.css
│  │  │  ├─ AudioHub.jsx
│  │  │  └─ AudioHub.test.js
│  │  ├─ customPlyBtn
│  │  │  ├─ customPlyBtn.css
│  │  │  ├─ CustomPlyBtn.jsx
│  │  │  └─ CustomPlyBtn.test.js
│  │  ├─ musicUploadForm
│  │  │  ├─ musicUploadForm.css
│  │  │  ├─ MusicUploadForm.jsx
│  │  │  └─ MusicUploadForm.test.js
│  │  ├─ playAllButton
│  │  │  ├─ playAllButton.css
│  │  │  ├─ PlayAllButton.jsx
│  │  │  └─ PlayAllButton.test.js
│  │  ├─ songList
│  │  │  ├─ songList.css
│  │  │  ├─ SongList.jsx
│  │  │  └─ SongList.test.js
│  │  └─ songRow
│  │     ├─ songRow.css
│  │     ├─ SongRow.jsx
│  │     └─ SongRow.test.js
│  ├─ features
│  │  └─ music
│  │     └─ musicSlice.js
│  ├─ global.css
│  ├─ images
│  │  └─ searchIcon.png
│  └─ index.js
└─ yarn.lock

### The state management approach.
I have used redux-toolkit for global state management and also react state(useState) for local state managment.

### Comments
.There are a few moment which needed to attention.
.I have used relative import so to avoid config errors with Jest and React test library.
.Because we did not have music Api,I have tried to get any info like song name etc from uploaded music.
.I have used uniqid instead of  chose element id like key.


