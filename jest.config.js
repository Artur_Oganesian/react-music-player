module.exports = {
  setupFilesAfterEnv: ["<rootDir>/node_modules/@testing-library/jest-dom"],
  testEnvironment: "jsdom",
  transform: {
    "^.+\\.[t|j]sx?$": "babel-jest",
  },
  moduleNameMapper: {
    "\\.(css)$": "identity-obj-proxy",
  },
};
